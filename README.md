# Kubernetes RabbitMQ Metrics Server

This is a sample project intended to demonstrate the structure and function of a small Service Oriented Architecture system. The frontend service is comprised of a simple Flask server, written in python, that is able to push messages to the RabbitMQ broker.

The goal is then to Horizontally Autoscale Pods containing celery worker service containers based on the number of messages queued on RabbitMQ, a metric exported by a containerized Prometheus service that is then aggregated and exposed by an implementation of Kubernetes' Custom Metrics API server.

Prerequisites
-------------
In order to run this project locally, you need to have Docker and Kubernetes installed on your machine.
Note that most of the API versions in this walkthrough target Kubernetes 1.9+. Note that current versions of the prometheus adapter *only* work with
Kubernetes 1.8+.

The Scenario
------------

Suppose that you are running a highly distributed system; a Python backend server that queues jobs / tasks to a RabbitMQ instance with a slew of celery workers at your command, ready to concurrently execute these jobs / tasks. It's ready to unveil to the world... except you're not entirely sure if just one lone worker instance will handle all of your impending
traffic once it goes viral. Thankfully, you've got Kubernetes.

Deploy the app and tech stack into your cluster and expose them via services so that you can
send traffic and fetch metrics from them. All of the  following files can be found in this project's `manifest` directory.

```shell
$ kubectl apply -f redis-configmap.yaml -f redis-deployment.yaml -f redis-svc.yaml # redis
$ kubectl apply -f rabbitmq-deployment.yaml -f rabbitmq-svc.yaml # rabbitMQ
$ kubectl apply -f localvolume-pvc.yaml -f flask-deployment.yaml -f flask-svc.yaml # flask server
$ kubectl apply -f celery-deployment.yaml # celery worker
```

##### Note: the flask server and celery worker code can be found in this project's `app` directory. It is mounted as a local persistent volume so you have to update the `spec.local.path` attribute found in `localvolume-pvc.yaml` in order to succesfully run this demo

Once everything is up and running, you should be able to send a request to the flask server to add a random assortment of tasks onto RabbitMQ:
```console
$ curl -X POST http://localhost:5000
{
  "msg": "Added 412 simple tasks and 49 delayed tasks to the queue."
}
```

The number of tasks specified by the response should be reflected by our RabbitMQ instance meaning that if we log onto the rabbitMQ management page at `http://localhost:15672` we should see, in my case, 461 queued messages. The count should steadily decreases as the celery workers execute each of these messages.

Great. We have succesfully kubernetisified our architecture, now what? Well, having to manually keep an eye on all this activity and having to spin up or decrease the number of celery workers instances accordingly is rather stressful so why not have Kubernetes handle it? It's purpose is to orchestrate distributed systems, such as this example, after all.

Before you can autoscale the celery workers, we need to make sure that Kubernetes can read the metrics that our tech stack exposes, specifically the number of queued messages on RabbitMQ.

### Launching Prometheus

First, you'll need to deploy the Prometheus Operator.  Check out this [getting started guide](https://coreos.com/operators/prometheus/docs/latest/user-guides/getting-started.html) for more information on the Prometheus Operator. This project already includes its own implementation of it so all you have to do is run the following commands. Again, the files are based from this project's `manifest` directory.

```shell
$ kubectl apply -f prometheus-operator.yaml
$ kubectl apply -f service-monitor.yaml -f prometheus-instance.yaml
```

##### Note: this project uses [kbudde/rabbitmq_exporter](https://github.com/kbudde/rabbitmq_exporter) to expose RabbitMQ metrics

In order to expose metrics beyond CPU and memory to Kubernetes for autoscaling, you'll need an "adapter" that serves the custom metrics API.
Since you have Prometheus collecting metrics, it makes sense to use the Prometheus adapter to serve metrics out of Prometheus.

### Launching the Adapter

Now that there's a running instance of Prometheus that's monitoring RabbitMQ, we will need to deploy the adapter, which knows how to communicate with both Kubernetes and Prometheus, acting as a translator
between the two. Run the following, which can be found in this project's `manifest` directory:

```shell
$ kubectl apply -f custom-metrics-namespace.yaml # create the 'custom-metrics' namespace
$ kubectl apply -f custom-metrics-configmap.yaml # discovers, associates, names, and queries the metrics
$ kubectl apply -f custom-metrics-server.yaml # launches the adapter / custom metrics api server
```

##### Note: if you launched the prometheus operator and instance on a different namespace, you need to update the `--prometheus-url` arg in `custom-metrics-server.yaml` to point to reflect that change

With that all set, your custom metrics API should show up in discovery. Try fetching the discovery information for it:

```console
$ kubectl get --raw /apis/custom.metrics.k8s.io/v1beta1
{"kind":"APIResourceList","apiVersion":"v1","groupVersion":"custom.metrics.k8s.io/v1beta1","resources":[{"name":"namespaces/rabbitmq_celery_queue_messages_ready","singularName":"","namespaced":false,"kind":"MetricValueList","verbs":["get"]},{"name":"pods/rabbitmq_celery_queue_messages_ready","singularName":"","namespaced":true,"kind":"MetricValueList","verbs":["get"]},{"name":"namespaces/rabbitmq_test_queue_messages_ready","singularName":"","namespaced":false,"kind":"MetricValueList","verbs":["get"]},{"name":"pods/rabbitmq_test_queue_messages_ready","singularName":"","namespaced":true,"kind":"MetricValueList","verbs":["get"]}]}
```

Once that is all up and running, we can launch a HorizontalPodAutoscaler to accomplish the celery worker autoscaling:

```shell
$ kubectl apply -f celery-hpa.yaml
```

##### Note: update the `describedObject.name` attribute in `celery-hpa.yaml` to point to the running RabbitMQ pod

Let's try sending a request to the Flask server again:
```console
$ curl -X POST http://localhost:5000
{
  "msg": "Added 384 simple tasks and 42 delayed tasks to the queue."
}
```

If we run `kubectl describe hpa`, we see:
```console
Events:
Type    Reason             Age               From                       Message
----    ------             ----              ----                       -------
Normal  SuccessfulRescale  3s (x2 over 44h)  horizontal-pod-autoscaler  New size: 4; reason: Pod metric rabbitmq_celery_queue_messages_ready above target
```

Let's double-check just to make sure.
```console
$ kubectl get pods
NAME                                   READY   STATUS    RESTARTS
celery-ddf5fbb96-7nxjt                 1/1     Running   0
celery-ddf5fbb96-82j7b                 1/1     Running   0
celery-ddf5fbb96-86v56                 1/1     Running   0
celery-ddf5fbb96-87szw                 1/1     Running   0
flask-app-79f7879959-xnh5q             1/1     Running   0
prometheus-main-0                      2/2     Running   0
prometheus-operator-787b5fcb98-jq4v9   1/1     Running   0
rabbitmq-7b8dbf6cbf-k9sdb              2/2     Running   0
redis-deployment-c4f9b78dc-wjcsg       1/1     Running   0
```
