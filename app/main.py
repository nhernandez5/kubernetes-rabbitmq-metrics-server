from factory import create_app, create_celery

def run():
    app = create_app()
    celery = create_celery(app)
    app.celery = celery
    app = app.load_routes(app) # HACK to avoid circular dependency

    if app.config.get("IS_DOCKER"):
        app.run(host="0.0.0.0")
    else:
        app.run()

if __name__ == "__main__":
    run()
