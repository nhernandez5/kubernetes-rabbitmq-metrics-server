from celery_worker_app.logging import init_celery_logging_system
from config.example import Config

from celery import Celery, Task
from flask import Flask
from logging.config import dictConfig
from redis import StrictRedis

def set_celery_context():
    init_celery_logging_system()

    from celery.concurrency import asynpool
    asynpool.PROC_ALIVE_TIMEOUT = 60.0

# HACK to avoid circular dependency
def load_routes(app):
    from server.routes import api
    app.register_blueprint(api)

    return app

def create_app():
    dictConfig(
        {
            "version": 1,
            "formatters": {"default": {"format": "%(asctime)s %(levelname)s: %(message)s"}},
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "formatter": "default",
                    "stream": "ext://sys.stdout",
                }
            },
            "root": {"level": "INFO", "handlers": ["console"]},
        }
    )

    app = Flask(__name__)  # pylint: disable=invalid-name
    app.config.from_object(Config)

    redis = StrictRedis(
        app.config.get("REDIS_HOST"), db=app.config.get("REDIS_DB"), decode_responses=True
    )  # pylint: disable=invalid-name
    app.redis = redis

    app.load_routes = load_routes

    return app

def create_celery(app=None):
    app = app or create_app()

    class WithAppContextTask(Task):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return Task.__call__(self, *args, **kwargs)

    set_celery_context()
    celery = Celery("kubernetes-rabbitmq-metrics",
        broker=Config.CELERY_BROKER,
        backend=Config.CELERY_RESULT_BACKEND)
    celery.config_from_object(Config)
    celery.Task = WithAppContextTask

    return celery
