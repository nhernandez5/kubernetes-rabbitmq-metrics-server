from factory import create_app, create_celery

app = create_app()
celery = create_celery(app)

if __name__ == '__main__':
    celery.start()
