import logging
import os
import socket
import sys

from billiard import current_process
from celery.utils.log import ColorFormatter
from celery._state import get_current_task
from logging.handlers import RotatingFileHandler

LOG_SIZE = 1024 * 1024 * 100  # 100MB
LOG_BACKUPS = 5
LOGGER_REGISTRY = {}

class TaskFormatter(ColorFormatter):
    def format(self, record):
        task = get_current_task()

        if task and task.request:
            self._fmt = "[%(asctime)s: %(levelname)s/%(processName)s-%(process)d] %(task_name)s[%(task_id)s] %(filename)s:%(lineno)d: %(message)s"
            record.__dict__.update(task_id=task.request.id, task_name=task.name)
        else:
            self._fmt = "[%(asctime)s: %(levelname)s/%(processName)s-%(process)d] %(filename)s:%(lineno)d: %(message)s"

        return ColorFormatter(fmt=self._fmt).format(record)

def init_celery_logging_system():
    if not LOGGER_REGISTRY:
        logger = logging.getLogger()
        log_dir = "/home/dev/logs" # relative to celery docker container

        if hasattr(current_process(), "index"):
            process_name = f"worker-{current_process().index}"
        else:
            process_name = "parent"

        log_filename = f"{socket.gethostname()}-{process_name}.out"

        # set log level
        logger.setLevel(logging.INFO)
        logging.getLogger("celery.task").setLevel(logging.INFO)

        # configure file handler
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)

        full_log_path = os.path.join(log_dir, log_filename)
        file_handler = RotatingFileHandler(full_log_path, "a", LOG_SIZE, LOG_BACKUPS)
        logger.addHandler(file_handler)

        # configure stream handler
        stdout_handler = logging.StreamHandler(stream=sys.stdout)
        stdout_handler.setFormatter(TaskFormatter())
        logging.getLogger().addHandler(stdout_handler)

        LOGGER_REGISTRY[None] = logger
