import random
import time

from celery.utils.log import get_task_logger
from factory import create_celery

celery = create_celery()
logger = get_task_logger(__name__)

@celery.task
def simple_random_math():
    x = random.randint(1, 10000)
    y = random.randint(1, 10000)

    addition = x + y
    multiplication = x * y

    logger.info(f"{x} + {y} is equal to {addition}")
    logger.info(f"{x} * {y} is equal to {multiplication}")

    return {"addition": addition, "multiplication": multiplication}

@celery.task
def delayed_random_math():
    x = random.randint(1, 10000)
    y = random.randint(1, 10000)

    delay = random.randint(15, 60)
    logger.info(f"There's going to be a {delay} second delay...")
    time.sleep(delay)

    addition = x + y
    multiplication = x * y

    logger.info(f"{x} + {y} is equal to {addition}")
    logger.info(f"{x} * {y} is equal to {multiplication}")

    return {"addition": addition, "multiplication": multiplication}
