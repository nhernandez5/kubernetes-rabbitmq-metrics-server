import random

from celery import group
from flask import Blueprint, jsonify, request

from celery_worker_app.tasks import simple_random_math, delayed_random_math

api = Blueprint("api", __name__)

@api.route("/", methods=["GET", "POST"])
def index():
    if request.method == 'GET':
        return jsonify({
            "msg": "Hello there! Not much to see here, send a post request to this endpoint to add tasks to RabbitMQ"
        }), 200

    tasks = []
    num_simple_tasks = random.randint(50, 500)
    num_delayed_tasks = random.randint(5, 50)

    for x in range(0, num_simple_tasks):
        tasks.append(simple_random_math.s())

    for y in range(0, num_delayed_tasks):
        tasks.append(delayed_random_math.s())

    job = group(tasks)
    job.apply_async()

    return jsonify({
        "msg": f"Added {num_simple_tasks} simple tasks and {num_delayed_tasks} delayed tasks to the queue."
    }), 202
