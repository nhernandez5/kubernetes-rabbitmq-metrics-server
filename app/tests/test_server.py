import json
import unittest

from factory import create_app
from redis import StrictRedis


class ServerTests(unittest.TestCase):
    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        app = create_app()
        app.config["TESTING"] = True
        app.config["WTF_CSRF_ENABLED"] = False
        app.config["DEBUG"] = False
        self.app = app.test_client()
        self.redis = StrictRedis(
            app.config.get("REDIS_HOST"), db=app.config.get("REDIS_DB"), decode_responses=True
        )  # pylint: disable=invalid-name

        self.assertEqual(app.debug, False)

    # executed after each test
    def tearDown(self):
        self.redis.flushdb()

    ###############
    #### tests ####
    ###############

    def test_main_page(self):
        response = self.app.get("/")

        self.assertEqual(response["code"], 200)

if __name__ == "__main__":
    unittest.main()
