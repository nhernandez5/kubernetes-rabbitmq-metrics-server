class Config(object):
    TASK_AMQP_INSTANCE = "amqp://redis:5672"

    REDIS_HOST = 'redis' # docker-compose redis image name
    REDIS_DB = '14'

    # celery config
    CELERY_ALWAYS_EAGER = False
    CELERYD_HIJACK_ROOT_LOGGER = False
    CELERY_SEND_EVENTS = False
    CELERY_ENABLE_REMOTE_CONTROL = False
    CELERY_BROKER = "amqp://rabbitmq:5672"
    CELERY_RESULT_BACKEND = "redis://redis:6379/15"
    CELERY_ROUTES = {}
    CELERY_IMPORTS = (
        "celery_worker_app.tasks"
    )

    IS_DOCKER = True
    DEBUG = True
    ENVIRONMENT = 'development'
